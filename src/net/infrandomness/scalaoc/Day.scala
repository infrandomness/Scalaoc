package net.infrandomness.scalaoc

import net.infrandomness.scalaoc.days.SonarSweep

trait Day {
  def run(arguments: Array[String]): Unit
}

object Day {
  def getDay(day: String): Option[Day] = day match
    case "1" => Some(SonarSweep())
    case _ => None
}