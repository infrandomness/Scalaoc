package net.infrandomness.scalaoc.utils

import scala.io.Source

object IO {
  def readFileToVec(path: String): Vector[String] =
    val source = Source.fromFile(path)
    val vec = source.getLines().toVector
    source.close()
    vec
}
