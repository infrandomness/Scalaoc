package net.infrandomness.scalaoc

import net.infrandomness.scalaoc.Day

object Main {
  def main(args: Array[String]): Unit =
    Day.getDay(args(0)).foreach(day => day.run(args.drop(1)))
}
